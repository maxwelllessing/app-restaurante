package br.com.fluttersakaesapp;

import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.BatteryManager;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.NonNull;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;


public class MainActivity extends FlutterActivity implements CallNotificationAppService {

    private NotificationAppService mService;

    private boolean mBound = false;

    private ServiceConnection connection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            mBinder = (NotificationAppService.LocalBinder) service;
            mBinder.registerListener(MainActivity.this);
            mService = mBinder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    @Override
    protected void onStop() {
        super.onStop();
        //if (mBinder != null)
        //      mBinder.unregisterListener();
        // unbindService(connection);
        // mBound = false;
    }

    @Override
    public void call() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // if (mResult != null) {
                // mResult.success(1);
                // mResult = null;
                //  }
                Log.d("SEND", "OK_SERVICE");
                methodChannel.invokeMethod("didRecieveTranscript", 0);
            }
        });

    }

    private MethodChannel.Result mResult;

    private MethodChannel methodChannel;

    private final String CHANNEL = "flutter.dev/battery";

    private NotificationAppService.LocalBinder mBinder = null;

    private final String CHANNEL2 = "wingquest.stablekernel.io/speech";

    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine);

       /* methodChannel = new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), CHANNEL2);

        final Intent intent = new Intent(this, NotificationAppService.class);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent);
        } else {
            startService(intent);
        }
        bindService(intent, connection, Context.BIND_AUTO_CREATE);

        new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), CHANNEL).setMethodCallHandler(new MethodChannel.MethodCallHandler() {
            @Override
            public void onMethodCall(@NonNull MethodCall call, @NonNull MethodChannel.Result result) {
                //mResult = result;
                //Intent intent = new Intent(MainActivity.this, NotificationAppService.class);
                //bindService(intent, connection, Context.BIND_AUTO_CREATE);
                if (call.method.equals("getBatteryLevel")) {

                    mResult = result;
                    //Intent intent = new Intent(MainActivity.this, NotificationAppService.class);
                    //bindService(intent, connection, Context.BIND_AUTO_CREATE);
                    //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    //    startForegroundService(intent);
                    // }else {
                    //    startService(intent);
                    //  }

                    // int batteryLevel = getBatteryLevel();

                    // if (batteryLevel != -1) {
                    // result.success(batteryLevel);
                    // } else {
                    //  result.error("UNAVAILABLE", "Battery level not available.", null);
                    // }
                } else {
                    result.notImplemented();
                }
            }
        });
    }

    private int getBatteryLevel() {
        int batteryLevel;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            BatteryManager batteryManager = (BatteryManager) getSystemService(Context.BATTERY_SERVICE);
            batteryLevel = batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
        } else {
            Intent intent = new ContextWrapper(getApplicationContext()).registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
            batteryLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) * 100 / intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        }
        return batteryLevel;
    }*/

    }
}
