package br.com.fluttersakaesapp;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import java.util.Timer;
import java.util.TimerTask;

public class NotificationAppService extends Service {

    private static Timer timer = new Timer();

    private CallNotificationAppService mListener = null;

    private final IBinder binder = new LocalBinder();

    public class LocalBinder extends Binder {

        public void registerListener(CallNotificationAppService listener) {
            mListener = listener;
        }

        public void unregisterListener() {
            mListener = null;
        }

        NotificationAppService getService() {
            return NotificationAppService.this;
        }

    }

    @Override
    public void onCreate() {

        super.onCreate();

        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {



                if (mListener != null)
                    mListener.call();

                Log.d("SEND_SERVICE_RUNNING", "OK_SERVICE");

            }
        }, 0, 10000);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }
}
