import 'dart:io';
import 'dart:math';

import 'package:fluttersakaesapp/models/item.dart';
import 'package:fluttersakaesapp/models/order.dart';
import 'package:fluttersakaesapp/util.dart';

class PrintSocket {
  static const s = " ";

  Future<bool> print(String print, String ip) async {
    bool ok = true;
    try {
      Socket socket = await Socket.connect(ip, 8888);
      socket.write(print);
      socket.close();
    } catch (e) {
      ok = false;
    }
    return ok;
  }

  Future<bool> printComanda(Order order, List<Item> items) async {
    List<int> printerGroup = [];

    for (int i = 0; i < items.length; i++) {
      if (items[i].printerId != null) {
        bool add = true;
        for (int p = 0; p < printerGroup.length; p++) {
          if (printerGroup[p] == items[i].printerId) {
            add = false;
            break;
          }
        }
        if (add) printerGroup.add(items[i].printerId);
      }
    }

    String stringToPrint;

    Item item;

    int hasItems;

    String ip;

    for (int p = 0; p < printerGroup.length; p++) {
      stringToPrint = "";
      stringToPrint += "<\\BOLD>" + paddingRight(order.typeDescription(), 64);
      stringToPrint += "\n";
      stringToPrint += "<\\BOLD>" + paddingRight("COMANDA", 32);
      stringToPrint += paddingLeft(Util.dateFormater(order.createdAt), 32);
      stringToPrint += "\n";
      if (order.table != null) {
        stringToPrint += paddingRight("MESA " + order.table.toString(), 64);
        stringToPrint += "\n";
      }
      stringToPrint += "<\\SEPARATOR>\n";
      stringToPrint += "<\\BOLD>" + paddingRight("ITEM", 60);
      stringToPrint += paddingLeft("QTDE", 4);
      stringToPrint += "\n";
      stringToPrint += "<\\SEPARATOR>\n";
      hasItems = 0;
      for (int i = 0; i < items.length; i++) {
        item = items[i];
        if (printerGroup[p] == item.printerId) {
          hasItems++;
          ip = item.ip;
          if (item.variances != null) {
            stringToPrint += paddingRight(item.name + " " + item.variances, 60);
            stringToPrint += paddingLeft(" " + item.amount.toString(), 4);
          } else {
            stringToPrint += paddingRight(item.name, 60);
            stringToPrint += paddingLeft(" " + item.amount.toString(), 4);
            if (item.items != null) {
              for (int c = 0; c < item.items.length; c++) {
                stringToPrint += "\n";
                String variances = item.items[c].variances != null
                    ? item.items[c].variances
                    : "";
                stringToPrint +=
                    paddingRight(item.items[c].name + " " + variances, 64);
              }
            }
          }
          stringToPrint += "\n";
          stringToPrint += "<\\SEPARATOR>\n";
        }
      }
      stringToPrint += "\n";
      if (hasItems > 0)
        return await print(Util.removerAcentos(stringToPrint), ip);
    }
    return true;
  }

  Future<bool> printPedido(Order order, List<Item> items) async {
    List<int> printerGroup = [];

    for (int i = 0; i < items.length; i++) {
      if (items[i].printerId != null) {
        bool add = true;
        for (int p = 0; p < printerGroup.length; p++) {
          if (printerGroup[p] == items[i].printerId) {
            add = false;
            break;
          }
        }
        if (add) printerGroup.add(items[i].printerId);
      }
    }

    String stringToPrint;

    Item item;

    int hasItems;

    String ip;

    for (int p = 0; p < printerGroup.length; p++) {
      stringToPrint = "";
      stringToPrint +=
          "<\\BOLD>" + paddingRight("PEDIDO " + order.id.toString(), 32);
      stringToPrint += paddingLeft(Util.dateFormater(order.createdAt), 32);
      stringToPrint += "\n";
      if (order.table != null) {
        stringToPrint += paddingRight("MESA " + order.table.toString(), 64);
        stringToPrint += "\n";
      }
      stringToPrint += "<\\SEPARATOR>\n";
      stringToPrint += "<\\BOLD>" + paddingRight("ITEM", 38);
      stringToPrint += paddingLeft("QTDE", 6);
      stringToPrint += paddingLeft("VLR UNIT", 10);
      stringToPrint += paddingLeft("VLR TOTAL", 10);
      stringToPrint += "\n";
      stringToPrint += "<\\SEPARATOR>\n";
      hasItems = 0;
      for (int i = 0; i < items.length; i++) {
        item = items[i];
        if (printerGroup[p] == item.printerId) {
          hasItems++;
          ip = item.ip;
          if (item.variances != null) {
            stringToPrint += paddingRight(item.name + " " + item.variances, 38);
            stringToPrint += paddingLeft(" " + item.amount.toString(), 6);
            stringToPrint += paddingLeft(
                "R\$" + Util.numberFormatSymbol(item.value, ""), 10);
            stringToPrint += paddingLeft(
                "R\$" + Util.numberFormatSymbol(item.value * item.amount, ""),
                10);
          } else {
            stringToPrint += paddingRight(item.name, 38);
            stringToPrint += paddingLeft(" " + item.amount.toString(), 6);
            stringToPrint += paddingLeft(
                "R\$" + Util.numberFormatSymbol(item.value, ""), 10);
            stringToPrint += paddingLeft(
                "R\$" + Util.numberFormatSymbol(item.value * item.amount, ""),
                10);
            if (item.items != null) {
              for (int c = 0; c < item.items.length; c++) {
                stringToPrint += "\n";
                String variances = item.items[c].variances != null
                    ? item.items[c].variances
                    : "";
                stringToPrint +=
                    paddingRight(item.items[c].name + " " + variances, 64);
              }
            }
          }
          stringToPrint += "\n";
          stringToPrint += "<\\SEPARATOR>\n";
        }
      }
      stringToPrint += "\n";
      stringToPrint += "<\\BOLD>" + paddingRight("SUBTOTAL", 32);
      stringToPrint +=
          paddingLeft("R\$" + Util.numberFormatSymbol(order.value, ""), 32);
      stringToPrint += "\n";
      stringToPrint += "<\\BOLD>" + paddingRight("DESCONTO", 32);
      stringToPrint +=
          paddingLeft("-R\$" + Util.numberFormatSymbol(order.discount, ""), 32);
      stringToPrint += "\n";
      stringToPrint += "<\\BOLD>" + paddingRight("VALOR TOTAL", 32);
      stringToPrint += paddingLeft(
          "R\$" + Util.numberFormatSymbol(order.value - order.discount, ""),
          32);
      stringToPrint += "\n";
      stringToPrint += "\n";
      if (order.name != null) {
        stringToPrint += order.name;
        stringToPrint += "\n";
      }
      if (order.address != null) {
        stringToPrint += order.address;
        stringToPrint += "\n";
      }
      stringToPrint += "\n";
      if (hasItems > 0)
        return await print(Util.removerAcentos(stringToPrint), ip);
    }
    return true;
  }

  String paddingRight(String _s, int length) {
    _s = _s.padRight(length, s);
    return _s.toUpperCase();
  }

  String paddingLeft(String _s, int length) {
    _s = _s.padLeft(length, s);
    return _s.toUpperCase();
  }

  String paddingRightChar(String _s, int length, String char) {
    _s = _s.padRight(length, char).toUpperCase();
    return _s;
  }

  String paddingLeftChar(String _s, int length, String char) {
    _s = _s.padLeft(length, char).toUpperCase();
    return _s;
  }
}
