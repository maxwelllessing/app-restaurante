import 'dart:convert';
import 'package:fluttersakaesapp/component/shared_preferences_helper.dart';
import 'package:http/http.dart' as http;
import '../constant.dart';
import '../models/item.dart';
import '../util.dart';

class Order {
  int id;
  double value;
  int table;
  int amountPeople;
  String comments;
  int status;
  int userId;
  DateTime createdAt;
  String uiid;
  int type;
  String name;
  String address;
  int userType;
  DateTime deletedAt;
  double discount;

  List<Item> items = [];

  Order(
      {int id,
      double value,
      int table,
      int amountPeople,
      String comments,
      int status,
      int userId,
      DateTime createdAt,
      String uiid,
      List<Item> items,
      int type,
      String name,
      String address,
      int userType,
      DateTime deletedAt,
      double discount}) {
    this.id = id;
    this.value = value;
    this.table = table;
    this.amountPeople = amountPeople;
    this.comments = comments;
    this.status = status;
    this.userId = userId;
    this.createdAt = createdAt;
    this.uiid = uiid;
    this.items = items;
    this.type = type;
    this.name = name;
    this.address = address;
    this.userType = userType;
    this.deletedAt = deletedAt;
    this.discount = discount;
  }

  factory Order.fromMap(Map<String, dynamic> json) {
    return Order(
      id: json['id'],
      value: double.parse(json['value']),
      table: json['table'] != null ? int.parse(json['table'].toString()) : null,
      amountPeople: json['amount_people'] != null
          ? int.parse(json['amount_people'].toString())
          : null,
      comments: json['comments'],
      status:
          json['status'] != null ? int.parse(json['status'].toString()) : null,
      userId:
          json['user_id'] != null ? int.parse(json['user_id'].toString()) : 0,
      createdAt: DateTime.parse(json['created_at']),
      uiid: json["uiid"],
      type: json['type'],
      items: Item.parseList(maps: json['items']),
      name: json['name'],
      address: json['address'],
      userType: json['user_type'],
      discount: double.parse(json['discount']),
    );
  }

  Map toJson() {
    List<Map> items =
        this.items != null ? this.items.map((i) => i.toJson()).toList() : null;

    return {
      "id": this.id,
      "value": Util.numberFormat(this.value),
      "table": this.table,
      "amount_people": this.amountPeople,
      "comments": this.comments,
      "status": this.status,
      "user_id": this.userId,
      "created_at": this.createdAt.toString(),
      "items": items,
      "uiid": this.uiid,
      "type": this.type,
      "name": this.name,
      "address": this.address,
      "user_type": this.userType,
      "deleted_at": this.deletedAt,
      "discount": Util.numberFormat(this.discount),
    };
  }

  static Order parse(String responseBody) {
    final parsed = jsonDecode(responseBody);
    return Order.fromMap(parsed);
  }

  static List<Order> parseList({String json, List<dynamic> maps}) {
    if (json != null) {
      final parsed = jsonDecode(json);
      return parsed.map<Order>((json) => Order.fromMap(json)).toList();
    } else {
      return maps.map<Order>((json) => Order.fromMap(json)).toList();
    }
  }

  static Future<Map<String, dynamic>> fetch() async {
    String apiToken;

    await SharedPreferencesHelper.instance()
        .then((value) => apiToken = value.getToken());

    final response = await http.get(
      Constant.URL + 'api/orders?api_token=' + apiToken,
      headers: {"Content-Type": "application/json", "token": apiToken},
    );
    if (response.statusCode == 200) {
      var body = jsonDecode(response.body);
      return {
        "hall": parseList(maps: body["hall"]),
        "delivery": parseList(maps: body["delivery"]),
        "takeaway": parseList(maps: body["takeaway"])
      };
    } else {
      return null;
    }
  }

  static Future<Order> send(Order order) async {
    String apiToken;

    await SharedPreferencesHelper.instance()
        .then((value) => apiToken = value.getToken());

    var body = jsonEncode(order.toJson());
    final response = await http.post(
        Constant.URL + 'api/order?api_token=' + apiToken,
        headers: {"Content-Type": "application/json", "token": apiToken},
        body: body);
    if (response.statusCode == 200) {
      return parse(response.body);
    } else {
      return null;
    }
  }

  String statusDescription() {
    if (this.status == 0) {
      return "O pedido está esperando confirmação";
    }
    if (this.status == 1) {
      return "O pedido está em preparo/pronto";
    }
    if (this.status == 2) {
      return "O pedido está em rota de entrega";
    }
    if (this.status == 3) {
      return "O pedido foi entregue";
    }
    return "O pedido foi cancelado pelo restaurante";
  }

  String typeDescription() {
    if (this.type == 0) {
      return "Salão";
    }
    if (this.type == 1) {
      return "Entrega";
    }
    return "Retirada";
  }
}
