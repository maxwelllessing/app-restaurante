import 'dart:convert';
import 'package:http/http.dart' as http;
import '../constant.dart';

class User {
  int id;
  String email;
  String password;
  String apiToken;

  User({int id, String email, String password, String apiToken}) {
    this.id = id;
    this.email = email;
    this.password = password;
    this.apiToken = apiToken;
  }

  factory User.fromMap(Map<String, dynamic> json) {
    return User(
        id: json['id'],
        email: json['email'],
        password: json['password'],
        apiToken: json['api_token']);
  }

  toJson() {
    return {
      "email": this.email,
      "password": this.password,
      "api_token": this.apiToken
    };
  }

  static User parse(String responseBody) {
    final parsed = jsonDecode(responseBody);
    return User.fromMap(parsed);
  }

  static Future<User> fetch(User user) async {
    final response = await http.post(Constant.URL + 'api/access',
        body: {"email": user.email, "password": user.password});
    if (response.statusCode == 200) {
      return parse(response.body);
    } else {
      return null;
    }
  }

  static Future<bool> fetchAccess(String apiToken) async {
    final response = await http.get(Constant.URL + 'api/access', headers: {"token" : apiToken});
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }
}
