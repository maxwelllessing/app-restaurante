import 'package:flutter/material.dart';
import 'package:fluttersakaesapp/constant.dart';
import '../models/order.dart';
import 'package:uuid/uuid.dart';

typedef AddNewOrderDialogNotification = void Function(Order order);

class AddNewOrderDialog extends StatefulWidget {
  final AddNewOrderDialogNotification addNewOrderDialogNotification;
  final String type;

  const AddNewOrderDialog(
      {Key key, this.addNewOrderDialogNotification, this.type})
      : super(key: key);

  @override
  AddNewOrderDialogState createState() =>
      new AddNewOrderDialogState(addNewOrderDialogNotification, type);
}

class AddNewOrderDialogState extends State<AddNewOrderDialog> {
  final AddNewOrderDialogNotification addNewOrderDialogNotification;

  List<DropdownMenuItem<String>> _dropDownTableAmoutPeople;
  List<DropdownMenuItem<String>> _dropDownType;

  String _amountPeople;

  String _table;

  String type;

  AddNewOrderDialogState(this.addNewOrderDialogNotification, this.type);

  final _txtNameController = TextEditingController();
  final _txtAddressController = TextEditingController();

  @override
  void initState() {
    super.initState();

    _dropDownTableAmoutPeople = getDropDownTableAmoutPeople();
    _table = _dropDownTableAmoutPeople[0].value;
    _amountPeople = _dropDownTableAmoutPeople[0].value;
  }

  List<DropdownMenuItem<String>> getDropDownTableAmoutPeople() {
    List<DropdownMenuItem<String>> items = new List();
    for (int i = 1; i < 99; i++) {
      items.add(new DropdownMenuItem(
          value: i.toString(), child: new Text(i.toString())));
    }
    return items;
  }

  void _notification() {
    Order order = new Order(
        value: 0,
        status: 0,
        uiid: Uuid().v4(),
        createdAt: DateTime.now(),
        items: [],
        discount: 0,
        type: int.parse(type));
    if (type == "0") {
      order.table = int.parse(_table);
      order.amountPeople = int.parse(_amountPeople);
    }
    if (type == "1") {
      order.name = _txtNameController.text;
      order.address = _txtAddressController.text;
    }
    if (type == "2") {
      order.name = _txtNameController.text;
      order.address = _txtAddressController.text;
    }
    addNewOrderDialogNotification.call(order);
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  dialogContent(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          decoration: new BoxDecoration(
            color: Colors.white,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(16), topLeft: Radius.circular(16)),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 10.0,
                offset: const Offset(0.0, 10.0),
              ),
            ],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min, // To make the card compact
            children: <Widget>[
              ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: double.infinity),
                  child: Container(
                      padding: EdgeInsets.all(8),
                      child: Text("Novo pedido",
                          textAlign: TextAlign.start,
                          style:
                              TextStyle(fontSize: 18.0, color: Colors.white)),
                      decoration: BoxDecoration(
                          color: Constant.colorAccentColor,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(16))))),
              SizedBox(height: 16.0),
              SingleChildScrollView(
                  // won't be scrollable
                  padding: EdgeInsets.only(left: 8, right: 8),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Visibility(
                          visible: type == "0",
                          maintainSize: false,
                          maintainInteractivity: false,
                          child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Selecione a mesa",
                                  textAlign: TextAlign.left,
                                ),
                                DropdownButton<String>(
                                    value: _table,
                                    items: _dropDownTableAmoutPeople,
                                    onChanged: (String value) {
                                      setState(() {
                                        _table = value;
                                      });
                                    },
                                    isExpanded: true),
                                Text("Selecione o nº de pessoas",
                                    textAlign: TextAlign.left),
                                DropdownButton<String>(
                                    value: _amountPeople,
                                    items: _dropDownTableAmoutPeople,
                                    onChanged: (String value) {
                                      setState(() {
                                        _amountPeople = value;
                                      });
                                    },
                                    isExpanded: true),
                              ])),
                      Visibility(
                          visible: type == "1" || type == "2",
                          maintainSize: false,
                          maintainInteractivity: false,
                          child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("Nome Cliente", textAlign: TextAlign.left),
                                TextField(controller: _txtNameController),
                              ])),
                      Visibility(
                          visible: type == "1",
                          maintainSize: false,
                          maintainInteractivity: false,
                          child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("Endereço Cliente",
                                    textAlign: TextAlign.left),
                                TextField(controller: _txtAddressController),
                              ])),
                    ],
                  )),
              SizedBox(height: 16.0),
              Align(
                  alignment: Alignment.bottomRight,
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        RaisedButton(
                          onPressed: () {
                            Navigator.of(context).pop(); // To close the dialog
                          },
                          child: Text("Cancelar"),
                        ),
                        SizedBox(width: 8.0),
                        RaisedButton(
                            onPressed: () {
                              _notification();
                              Navigator.of(context).pop();
                            },
                            child: Text("Confirmar")),
                        SizedBox(width: 8.0),
                      ]))
            ],
          ),
        ),
      ],
    );
  }
}
