import 'package:flutter/material.dart';
import 'package:fluttersakaesapp/constant.dart';
import '../models/order.dart';
import 'package:uuid/uuid.dart';

typedef CancelOrderDialogNotification = void Function(Order order);

class CancelOrderDialog extends StatefulWidget {
  final CancelOrderDialogNotification cancelOrderDialogNotification;
  final Order order;

  const CancelOrderDialog(
      {Key key, this.cancelOrderDialogNotification, this.order})
      : super(key: key);

  @override
  CancelOrderDialogState createState() =>
      new CancelOrderDialogState(cancelOrderDialogNotification, order);
}

class CancelOrderDialogState extends State<CancelOrderDialog> {
  final CancelOrderDialogNotification cancelOrderDialogNotification;

  final Order order;

  CancelOrderDialogState(this.cancelOrderDialogNotification, this.order);

  @override
  void initState() {
    super.initState();
  }

  void _notification() {
    order.status = 4;
    Order.send(order).then((value) => {
          cancelOrderDialogNotification.call(order),
          Navigator.of(context).pop(),
        });
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  dialogContent(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          decoration: new BoxDecoration(
            color: Colors.white,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(16), topLeft: Radius.circular(16)),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 10.0,
                offset: const Offset(0.0, 10.0),
              ),
            ],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min, // To make the card compact
            children: <Widget>[
              ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: double.infinity),
                  child: Container(
                      padding: EdgeInsets.all(8),
                      child: Text("Cancelar pedido " + order.id.toString(),
                          textAlign: TextAlign.start,
                          style:
                              TextStyle(fontSize: 18.0, color: Colors.white)),
                      decoration: BoxDecoration(
                          color: Constant.colorAccentColor,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(16))))),
              SizedBox(height: 16.0),
              SingleChildScrollView(
                  // won't be scrollable
                  padding: EdgeInsets.only(left: 8, right: 8),
                  child: Text("Tem certeza que dejesa cancelar este pedido?")),
              SizedBox(height: 16.0),
              Align(
                  alignment: Alignment.bottomRight,
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        RaisedButton(
                          onPressed: () {
                            Navigator.of(context).pop(); // To close the dialog
                          },
                          child: Text("Cancelar"),
                        ),
                        SizedBox(width: 8.0),
                        RaisedButton(
                            onPressed: () {
                              _notification();
                            },
                            child: Text("Confirmar")),
                        SizedBox(width: 8.0),
                      ]))
            ],
          ),
        ),
      ],
    );
  }
}
