import 'package:flutter/material.dart';
import 'package:fluttersakaesapp/models/variance.dart';
import '../constant.dart';
import '../models/product.dart';
import '../models/product.dart';
import '../models/variance.dart';
import '../util.dart';

class RowVarianceCheckboxScreen extends StatelessWidget  {

  final RowVarianceCheckboxScreenCallback _checkboxScreenCallback;

  final Variance variance;

  final Product product;

  final int index;

  final int indexGroup;

  final int indexGroupValue;

  RowVarianceCheckboxScreen(this.variance, this.product, this.index, this.indexGroup, this.indexGroupValue, this._checkboxScreenCallback);

  @override
  Widget build(BuildContext context) {
    return Row(children: [
              Expanded(
                flex: 8, // 20%
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(variance.name.toString(),
                          textAlign: TextAlign.start,
                          softWrap: true,
                          style: TextStyle(fontSize: 14)),
                      Visibility(
                          maintainInteractivity: false,
                          maintainSize: false,
                          maintainSemantics: false,
                          maintainAnimation: false,
                          maintainState: false,
                          visible: product.type == 1,
                          child: Text(Util.numberFormatText(variance.value),
                              textAlign: TextAlign.start,
                              softWrap: true,
                              style: TextStyle(fontSize: 14)))
                    ]),
              ),
              Spacer(),
              Expanded(
                  flex: 2, // 20%
                  child: new Radio(
                    value: index,
                    groupValue: indexGroupValue,
                    onChanged: (int value) {
                      _checkboxScreenCallback(indexGroup, value);
                    },
                  ))
            ]);
  }
}

typedef RowVarianceCheckboxScreenCallback = void Function(int indexGroup, int value);
