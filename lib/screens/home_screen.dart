import 'dart:async';

import 'package:background_fetch/background_fetch.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:fluttersakaesapp/component/shared_preferences_helper.dart';
import 'package:fluttersakaesapp/screens/add_new_order_dialog.dart';
import 'package:fluttersakaesapp/screens/cancel_order_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../constant.dart';
import '../models/order.dart';
import 'package:intl/intl.dart';
import 'package:uuid/uuid.dart';
import 'dart:math' as math;
import '../component/print_socket.dart';
import 'package:progress_indicators/progress_indicators.dart';

class HomeScreen extends StatefulWidget {
  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<HomeScreen> with SingleTickerProviderStateMixin {
  List<Order> ordersHall = [];
  List<Order> ordersDelivery = [];
  List<Order> ordersTakeaway = [];

  bool isLoading = true;

  List<DropdownMenuItem<String>> _dropDownTableAmoutPeople;
  List<DropdownMenuItem<String>> _dropDownType;

  String _amountPeople;

  String _table;

  String _type;

  Timer timer;

  final _txtNameController = TextEditingController();
  final _txtAddressController = TextEditingController();

  bool isLoadingCancelar = false;

  TabController _tabController;

  int selectedMenu = 0;

  @override
  void initState() {
    super.initState();

    setupList();

    _dropDownTableAmoutPeople = getDropDownTableAmoutPeople();
    _table = _dropDownTableAmoutPeople[0].value;
    _amountPeople = _dropDownTableAmoutPeople[0].value;
    //_dropDownType = getdropDownType();
    _type = "0";

    _tabController = new TabController(vsync: this, length: 3);

    timer =
        new Timer.periodic(Duration(seconds: 10), (Timer t) => {setupList()});
  }

  Future onSelectNotification(String payload) {
    debugPrint("payload : $payload");
    showDialog(
      context: context,
      builder: (_) => new AlertDialog(
        title: new Text('Notification'),
        content: new Text('$payload'),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return isLoading
        ? Scaffold(
            body: Center(
                child: JumpingText('...',
                    style:
                        TextStyle(fontSize: 50, fontWeight: FontWeight.bold))))
        : Scaffold(
            appBar: AppBar(
              bottom: PreferredSize(
                  preferredSize: const Size.fromHeight(40.0),
                  child: Theme(
                      data:
                          Theme.of(context).copyWith(accentColor: Colors.white),
                      child: TabBar(
                        onTap: (value) {
                          _type = value.toString();
                        },
                        controller: _tabController,
                        tabs: [
                          Tab(
                              child: Text("Salão",
                                  style: TextStyle(fontSize: 18))),
                          Tab(
                              child: Text("Entrega",
                                  style: TextStyle(fontSize: 18))),
                          Tab(
                              child: Text("Retirada",
                                  style: TextStyle(fontSize: 18))),
                        ],
                      ))),
              title: Text('Sakae\'s Pedidos'),
            ),
            body: TabBarView(
              controller: _tabController,
              children: [
                ordersHall.length <= 0
                    ? Center(
                        child:
                            Text("Sem pedidos", style: TextStyle(fontSize: 18)))
                    : Center(child: _buildList(ordersHall)),
                ordersDelivery.length <= 0
                    ? Center(
                        child:
                            Text("Sem pedidos", style: TextStyle(fontSize: 18)))
                    : Center(child: _buildList(ordersDelivery)),
                ordersTakeaway.length <= 0
                    ? Center(
                        child:
                            Text("Sem pedidos", style: TextStyle(fontSize: 18)))
                    : Center(child: _buildList(ordersTakeaway))
              ],
            ),
            bottomNavigationBar: _buildButton(),
            drawer: Drawer(
              // Add a ListView to the drawer. This ensures the user can scroll
              // through the options in the drawer if there isn't enough vertical
              // space to fit everything.
              child: ListView(
                // Important: Remove any padding from the ListView.
                padding: EdgeInsets.zero,
                children: <Widget>[
                  SizedBox(height: 48.0),
                  ListTile(
                    title: Text(
                      "Sakae\'s",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 24),
                    ),
                    onTap: () {
                      setState(() {
                        selectedMenu = 0;
                      });
                    },
                  ),
                  Padding(
                      child: Divider(),
                      padding: EdgeInsets.only(left: 8, right: 8)),
                  ListTile(
                    title: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(4)),
                            color: selectedMenu == 0
                                ? Constant.colorAccentColor
                                : null),
                        child: Padding(
                            padding: EdgeInsets.all(16),
                            child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Icon(
                                    Icons.home,
                                    color: selectedMenu == 0
                                        ? Colors.white
                                        : Colors.black,
                                    size: 20.0,
                                  ),
                                  Padding(
                                      padding: EdgeInsets.only(left: 8),
                                      child: Text("Inicio",
                                          style: TextStyle(
                                              color: selectedMenu == 0
                                                  ? Colors.white
                                                  : Colors.black)))
                                ]))),
                    onTap: () {
                      setState(() {
                        selectedMenu = 0;
                      });
                    },
                  ),
                  ListTile(
                    title: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(4)),
                            color: selectedMenu == 1
                                ? Constant.colorAccentColor
                                : null),
                        child: Padding(
                            padding: EdgeInsets.all(16),
                            child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Icon(
                                    Icons.attach_money,
                                    color: selectedMenu == 1
                                        ? Colors.white
                                        : Colors.black,
                                    size: 20.0,
                                  ),
                                  Padding(
                                      padding: EdgeInsets.only(left: 8),
                                      child: Text("Caixa",
                                          style: TextStyle(
                                              color: selectedMenu == 1
                                                  ? Colors.white
                                                  : Colors.black)))
                                ]))),
                    onTap: () {
                      setState(() {
                        selectedMenu = 1;
                      });
                    },
                  ),
                ],
              ),
            ));
  }

  Widget _buildButton() {
    return Padding(
        padding: EdgeInsets.all(16),
        child: RaisedButton(
          onPressed: () {
            _displayDialog2(context);
            //infoDialog(context, "teste");
            //showReview2(
            //context,
            //);
          },
          child: Padding(
              padding: EdgeInsets.all(16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                // Replace with a Row for horizontal icon + text
                children: <Widget>[
                  Text("Novo pedido", style: TextStyle(fontSize: 18))
                ],
              )),
          elevation: 5,
        ));
  }

  Widget _buildList(List<Order> list) {
    return ListView.separated(
      separatorBuilder: (context, index) => Divider(),
      padding: EdgeInsets.all(10.0),
      itemCount: list.length,
      itemBuilder: (BuildContext context, int index) {
        return InkWell(
            child: Padding(
                padding: EdgeInsets.all(8), child: _buildRow(list[index])),
            onTap: () => {
                  if (list[index].type == 0)
                    {
                      Navigator.pushNamed(context, 'product',
                          arguments: {'order': list[index]})
                    }
                  else
                    {
                      Navigator.pushNamed(context, 'details',
                          arguments: {'order': list[index]})
                    }
                },
            onLongPress: () => {_displayDialogOpcoes(context, list[index])});
      },
    );
  }

  _buildRow(Order order) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(order.typeDescription(),
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
          Row(children: [
            Visibility(
                visible: order.table != null,
                maintainSize: false,
                maintainInteractivity: false,
                child: Text("Mesa nº" + order.table.toString(),
                    textAlign: TextAlign.start,
                    style: TextStyle(fontSize: 18))),
            Spacer(),
            Visibility(
                visible: order.amountPeople != null,
                maintainSize: false,
                maintainInteractivity: false,
                child: Text("Nº" + order.amountPeople.toString() + " pessoas",
                    style: TextStyle(fontSize: 18)))
          ]),
          Row(children: <Widget>[
            Text(DateFormat("dd/MM/yyyy HH:mm:ss").format(order.createdAt),
                style: TextStyle(fontSize: 18)),
            Spacer(),
            Text(
                NumberFormat.currency(locale: "pt-BR", symbol: "R\$")
                    .format(order.value),
                style: TextStyle(fontSize: 18)),
          ]),
          Text(order.statusDescription(), style: TextStyle(fontSize: 18))
        ]);
  }

  List<DropdownMenuItem<String>> getDropDownTableAmoutPeople() {
    List<DropdownMenuItem<String>> items = new List();
    for (int i = 1; i < 99; i++) {
      items.add(new DropdownMenuItem(
          value: i.toString(), child: new Text(i.toString())));
    }
    return items;
  }

  List<DropdownMenuItem<String>> getdropDownType() {
    List<DropdownMenuItem<String>> items = new List();

    items.add(
        new DropdownMenuItem(value: 0.toString(), child: new Text("Salão")));

    items.add(
        new DropdownMenuItem(value: 1.toString(), child: new Text("Entrega")));

    items.add(
        new DropdownMenuItem(value: 2.toString(), child: new Text("Retirada")));

    return items;
  }

  _displayDialog(BuildContext context) async {
    return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Container(child: Text('Novo pedido')),
            content: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
              return SingleChildScrollView(
                  // won't be scrollable
                  child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  /*Text(
                    "Selecione o tipo de pedido",
                    textAlign: TextAlign.left,
                  ),
                  DropdownButton<String>(
                      value: _type,
                      items: _dropDownType,
                      onChanged: (String value) {
                        setState(() {
                          _type = value;
                        });
                      },
                      isExpanded: true),*/
                  Visibility(
                      visible: _type == "0",
                      maintainSize: false,
                      maintainInteractivity: false,
                      child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Selecione a mesa",
                              textAlign: TextAlign.left,
                            ),
                            DropdownButton<String>(
                                value: _table,
                                items: _dropDownTableAmoutPeople,
                                onChanged: (String value) {
                                  setState(() {
                                    _table = value;
                                  });
                                },
                                isExpanded: true),
                            Text("Selecione o nº de pessoas",
                                textAlign: TextAlign.left),
                            DropdownButton<String>(
                                value: _amountPeople,
                                items: _dropDownTableAmoutPeople,
                                onChanged: (String value) {
                                  setState(() {
                                    _amountPeople = value;
                                  });
                                },
                                isExpanded: true),
                          ])),
                  Visibility(
                      visible: _type == "1" || _type == "2",
                      maintainSize: false,
                      maintainInteractivity: false,
                      child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text("Nome Cliente", textAlign: TextAlign.left),
                            TextField(controller: _txtNameController),
                          ])),
                  Visibility(
                      visible: _type == "1",
                      maintainSize: false,
                      maintainInteractivity: false,
                      child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text("Endereço Cliente", textAlign: TextAlign.left),
                            TextField(controller: _txtAddressController),
                          ])),
                ],
              ));
            }),
            actions: <Widget>[
              new RaisedButton(
                child: new Text('CANCELAR'),
                onPressed: () {
                  Navigator.of(context).pop();
                  _amountPeople = null;
                  _table = null;
                },
              ),
              new RaisedButton(
                child: new Text('CONFIRMAR'),
                onPressed: () {
                  if (_amountPeople != null && _table != null) {
                    SharedPreferencesHelper.instance().then(
                        (value) => {navegationToProduct(value.getUserId())});
                  }
                },
              )
            ],
          );
        });
  }

  _displayDialog2(BuildContext context) async {
    return showDialog(
      context: context,
      builder: (BuildContext context) => AddNewOrderDialog(
          addNewOrderDialogNotification: (order) => {
                SharedPreferencesHelper.instance().then((value) => {
                      order.userId = int.parse(value.getUserId()),
                      Navigator.pushNamed(context, 'product',
                          arguments: {'order': order})
                    })
              },
          type: _type),
    );
  }

  _displayDialogOpcoes(BuildContext context, Order order) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Container(child: Text('Menu Opções')),
            content: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
              return SingleChildScrollView(
                  // won't be scrollable
                  child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  RaisedButton(
                    onPressed: () {
                      PrintSocket()
                          .printComanda(order, order.items)
                          .then((value) => {
                                //if (value) {finish()} else {isLoadingButton = false}
                              });
                    },
                    child: Padding(
                        padding: EdgeInsets.all(16),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          // Replace with a Row for horizontal icon + text
                          children: <Widget>[
                            Text("Imprimir comanda",
                                style: TextStyle(fontSize: 18))
                          ],
                        )),
                    elevation: 5,
                  ),
                  Padding(
                      padding: EdgeInsets.only(top: 8),
                      child: RaisedButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                          _displayDialogCancelar(context, order);
                        },
                        child: Padding(
                            padding: EdgeInsets.all(16),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              // Replace with a Row for horizontal icon + text
                              children: <Widget>[
                                Text("Cancelar pedido",
                                    style: TextStyle(fontSize: 18))
                              ],
                            )),
                        elevation: 5,
                      ))
                ],
              ));
            }),
            actions: <Widget>[],
          );
        });
  }

  _displayDialogCancelar(BuildContext context, Order order) async {
    return showDialog(
      context: context,
      builder: (BuildContext context) => CancelOrderDialog(
          cancelOrderDialogNotification: (order) => {setupList()},
          order: order),
    );
  }

  void navegationToProduct(userId) {
    Order order = new Order(
        value: 0,
        status: 0,
        uiid: Uuid().v4(),
        createdAt: DateTime.now(),
        userId: int.parse(userId),
        items: [],
        type: int.parse(_type));
    if (_type == "0") {
      order.table = int.parse(_table);
      order.amountPeople = int.parse(_amountPeople);
    }
    if (_type == "1") {
      order.name = _txtNameController.text;
      order.address = _txtAddressController.text;
    }
    if (_type == "2") {
      order.name = _txtNameController.text;
      order.address = _txtAddressController.text;
    }
    Navigator.pushNamed(context, 'product', arguments: {'order': order});
  }

  void setupList() async {
    isLoading = true;

    Order.fetch().then((value) => {
          if (mounted)
            {
              setState(() {
                ordersHall = value["hall"];
                ordersDelivery = value["delivery"];
                ordersTakeaway = value["takeaway"];
                isLoading = false;
              })
            }
        });
  }
}
