import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttersakaesapp/component/print_socket.dart';
import 'package:fluttersakaesapp/models/notification_message.dart';
import '../models/item.dart';
import '../models/order.dart';
import 'package:intl/intl.dart';
import 'dart:math' as math;
import 'package:progress_indicators/progress_indicators.dart';
import '../util.dart';
import '../screens/home_screen.dart';
import '../screens/product_screen.dart';
import '../screens/add_item_discount_dialog.dart';

// This is the type used by the popup menu below.
enum MenuOptions { cancel, discount, removeDiscount }

class DetailsScreen extends StatefulWidget {
  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  @override
  _DetailsState createState() => _DetailsState();
}

class _DetailsState extends State<DetailsScreen> {
  List<Item> items = [];

  Order order;

  final txtCommentsController = TextEditingController();

  bool isLoadingButton = false;

  ProductScreenNotificationOrder _notificationOrder;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final Map arguments = ModalRoute.of(context).settings.arguments as Map;

    if (arguments != null) {
      order = arguments['order'];
      _notificationOrder = arguments['notificationOrder'];
    }

    setupList();

    return Scaffold(
      appBar: AppBar(
        title: Text("Pedido da Mesa Nº" + order.table.toString()),
      ),
      body: Container(
        child: CustomScrollView(
          slivers: <Widget>[
            _buildList(items),
            SliverList(
              delegate: SliverChildListDelegate(
                [Divider(), _buildSubtotal(), _buildDiscount(), _buildTotal()],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: _buildButton(),
    );
  }

  Widget _buildButton() {
    return Padding(
        padding: EdgeInsets.all(16),
        child: RaisedButton(
          onPressed: () {
            if (isLoadingButton) return;

            isLoadingButton = true;

            order.comments = txtCommentsController.text;

            List<Item> printItems = [];

            for (int i = 0; i < order.items.length; i++) {
              Item e = order.items[i];
              if (e.status == 0) {
                printItems.add(e);
              }
            }

            if (printItems.length > 0) {
              PrintSocket().printComanda(order, printItems).then((value) => {
                    //if (value) {finish()} else {isLoadingButton = false}
                  });
            } else if (order.status == 1) {
              PrintSocket().printPedido(order, order.items).then((value) => {
                    //if (value) {finish()} else {isLoadingButton = false}
                  });
            }
            finish();
          },
          child: Padding(
              padding: EdgeInsets.all(16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                // Replace with a Row for horizontal icon + text
                children: <Widget>[
                  isLoadingButton
                      ? JumpingText('...', style: TextStyle(fontSize: 18))
                      : Text(_buttonText(), style: TextStyle(fontSize: 18))
                ],
              )),
          elevation: 5,
        ));
  }

  void finish() {
    changeStatus();

    setState(() {
      order = order;
    });

    Order.send(order).then((value) => {
          isLoadingButton = false,
          if (value != null)
            {
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => HomeScreen()),
                  (Route<dynamic> route) => false)
            }
        });
  }

  String _buttonText() {
    if (order.type == 0) {
      if (order.status == 1) return "Pagar";
      if (order.status == 0) return "Efetuar pedido";
    }
    if (order.type == 1) {
      if (order.status == 2) return "Pedido entregue";
      if (order.status == 1) return "Enviar pedido para entrega";
      if (order.status == 0) return "Efetuar pedido";
    }
    if (order.type == 2) {
      if (order.status == 1) return "Pagar";
      if (order.status == 0) return "Efetuar pedido";
    }
    return "";
  }

  void changeStatus() {
    if (order.type == 0) {
      if (order.status == 1) order.status = 3;
      if (order.status == 0) order.status = 1;
    }
    if (order.type == 1) {
      if (order.status == 2) order.status = 3;
      if (order.status == 1) order.status = 2;
      if (order.status == 0) order.status = 1;
    }
    if (order.type == 2) {
      if (order.status == 1) order.status = 3;
      if (order.status == 0) order.status = 1;
    }
    if (order.userType == 2) {
      NotificationMessage.sendNotification(new NotificationMessage(
          title: "Pedido nº" + order.id.toString(),
          notification: order.statusDescription(),
          action: "orders",
          userId: order.userId));
    }
    for (int i = 0; i < order.items.length; i++) {
      Item e = order.items[i];
      if (e.status == 1) e.status = 2;
      if (e.status == 0) {
        e.status = 1;
      }
    }
  }

  Widget _buildTotal() {
    return Padding(
        padding: EdgeInsets.only(left: 16, right: 16),
        child: Row(
          children: <Widget>[
            Text("Total",
                textAlign: TextAlign.start,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
            Spacer(),
            Text(Util.numberFormatText(order.value - order.discount),
                style: TextStyle(fontSize: 18))
          ],
        ));
  }

  Widget _buildSubtotal() {
    return Padding(
        padding: EdgeInsets.only(left: 16, right: 16),
        child: Row(
          children: <Widget>[
            Text("Subtotal",
                textAlign: TextAlign.start,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
            Spacer(),
            Text(Util.numberFormatText(order.value),
                style: TextStyle(fontSize: 18))
          ],
        ));
  }

  Widget _buildDiscount() {
    return Padding(
        padding: EdgeInsets.only(left: 16, right: 16),
        child: Row(
          children: <Widget>[
            Text("Desconto",
                textAlign: TextAlign.start,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
            Spacer(),
            Text("-" + Util.numberFormatText(order.discount),
                style: TextStyle(fontSize: 18))
          ],
        ));
  }

  _displayAddDiscountDialog(BuildContext context, Item item, int index) async {
    return showDialog(
      context: context,
      builder: (BuildContext context) => AddItemDiscountDialog(
        addItemDiscountDialogNotification: (value) =>
            {_discountItem(index, item, value)},
        valueProduct: item.amount * item.value,
      ),
    );
  }

  Widget _buildList(List<Item> list) {
    return SliverList(
        delegate: SliverChildBuilderDelegate(
      (BuildContext context, int index) {
        final int itemIndex = index ~/ 2;
        Item item = list[itemIndex];
        if (index.isEven) {
          return InkWell(
              child: Padding(
                  padding: EdgeInsets.all(16),
                  child: _buildRow(itemIndex, item)));
        }
        return Divider();
      },
      semanticIndexCallback: (Widget widget, int localIndex) {
        if (localIndex.isEven) {
          return localIndex ~/ 2;
        }
        return null;
      },
      childCount: math.max(0, list.length * 2 - 1),
    ));
  }

  Row _buildRow(int index, Item item) {
    return Row(children: [
      Expanded(
          flex: 6, // 20%
          child: Text(item.amount.toString() + "x " + item.name.toString(),
              textAlign: TextAlign.start,
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold))),
      Expanded(
          flex: 3, // 20%
          child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Text(Util.numberFormatText(item.value * item.amount),
                    style: TextStyle(fontSize: 18)),
                Visibility(
                    visible: item.discount > 0,
                    child: Text("-" + Util.numberFormatText(item.discount),
                        style: TextStyle(fontSize: 14)))
              ])),
      Expanded(
          flex: 1, // 20%
          child: PopupMenuButton<MenuOptions>(
            onSelected: (MenuOptions result) {
              switch (result) {
                case MenuOptions.cancel:
                  _removeItem(index, item);
                  break;
                case MenuOptions.discount:
                  _displayAddDiscountDialog(context, item, index);
                  break;
                case MenuOptions.removeDiscount:
                  _removeDiscountItem(index, item, item.discount);
                  break;
              }
            },
            itemBuilder: (BuildContext context) =>
                <PopupMenuEntry<MenuOptions>>[
              item.discount > 0
                  ? const PopupMenuItem<MenuOptions>(
                      value: MenuOptions.removeDiscount,
                      child: Text('Remover desconto'),
                    )
                  : const PopupMenuItem<MenuOptions>(
                      value: MenuOptions.discount,
                      child: Text('Desconto'),
                    ),
              order.status == 0
                  ? const PopupMenuItem<MenuOptions>(
                      value: MenuOptions.cancel,
                      child: Text('Cancelar'),
                    )
                  : null,
            ],
          )),
    ]);
  }

  _removeItem(int index, Item item) {
    order.value -= item.value * item.amount;
    order.items.removeAt(index);
    setupList();
    _notificationOrder(order);
    if (order.items.length <= 0) {
      Navigator.of(context).pop();
    }
  }

  _discountItem(int index, Item item, double value) {
    order.discount += value;
    item.discount += value;

    setupList();

    _notificationOrder(order);
  }

  _removeDiscountItem(int index, Item item, double value) {
    order.discount -= value;
    item.discount -= value;

    setupList();

    _notificationOrder(order);
  }

  void setupList() async {
    setState(() {
      items = order.items;
    });
  }
}
