import 'package:flutter/material.dart';
import 'package:fluttersakaesapp/constant.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:fluttersakaesapp/util.dart';

typedef AddItemDiscountDialogNotification = void Function(double value);

class AddItemDiscountDialog extends StatefulWidget {
  final AddItemDiscountDialogNotification addItemDiscountDialogNotification;
  final double valueProduct;

  const AddItemDiscountDialog(
      {Key key, this.addItemDiscountDialogNotification, this.valueProduct})
      : super(key: key);

  @override
  AddItemDiscountDialogState createState() => new AddItemDiscountDialogState(
      addItemDiscountDialogNotification, this.valueProduct);
}

class AddItemDiscountDialogState extends State<AddItemDiscountDialog> {
  final AddItemDiscountDialogNotification addItemDiscountDialogNotification;

  AddItemDiscountDialogState(
      this.addItemDiscountDialogNotification, this.valueProduct);

  final _txtValueController = new MoneyMaskedTextController(
      decimalSeparator: ',', thousandSeparator: '.');

  int groupValue = 0;

  final double valueProduct;

  @override
  void initState() {
    super.initState();

    _txtValueController.addListener(() {
      double _value = Util.numberDeformat(_txtValueController.text);
      if (groupValue == 1) {
        if (_value > valueProduct)
          _txtValueController.text = Util.numberFormat(valueProduct);
      } else {
        if (_value > 100) _txtValueController.text = Util.numberFormat(100);
      }
    });
  }

  void _notification() {
    double discount = Util.numberDeformat(_txtValueController.text);
    if (groupValue == 0) {
      discount = valueProduct * (discount / 100);
    }
    addItemDiscountDialogNotification.call(discount);
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  dialogContent(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          decoration: new BoxDecoration(
            color: Colors.white,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(16), topLeft: Radius.circular(16)),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 10.0,
                offset: const Offset(0.0, 10.0),
              ),
            ],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min, // To make the card compact
            children: <Widget>[
              ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: double.infinity),
                  child: Container(
                      padding: EdgeInsets.all(8),
                      child: Text("Adicionar desconto",
                          textAlign: TextAlign.start,
                          style:
                              TextStyle(fontSize: 18.0, color: Colors.white)),
                      decoration: BoxDecoration(
                          color: Constant.colorAccentColor,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(16))))),
              SizedBox(height: 16.0),
              SingleChildScrollView(
                  // won't be scrollable
                  padding: EdgeInsets.only(left: 8, right: 8),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Selecione o desconto",
                        textAlign: TextAlign.left,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          new Radio(
                            value: 0,
                            groupValue: groupValue,
                            onChanged: (v) {
                              setState(() {
                                groupValue = v;
                              });
                            },
                          ),
                          new Text(
                            'Percentual %',
                            style: new TextStyle(fontSize: 14.0),
                          ),
                          new Radio(
                            value: 1,
                            groupValue: groupValue,
                            onChanged: (v) {
                              setState(() {
                                groupValue = v;
                              });
                            },
                          ),
                          new Text(
                            'Valor (R\$)',
                            style: new TextStyle(
                              fontSize: 14.0,
                            ),
                          ),
                        ],
                      ),
                      //Text("Digite ", textAlign: TextAlign.left),
                      TextField(
                          controller: _txtValueController,
                          decoration: InputDecoration(
                              border: InputBorder.none, hintText: '0,00'),
                          keyboardType:
                              TextInputType.numberWithOptions(decimal: true))
                    ],
                  )),
              SizedBox(height: 16.0),
              Align(
                  alignment: Alignment.bottomRight,
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        RaisedButton(
                          onPressed: () {
                            Navigator.of(context).pop(); // To close the dialog
                          },
                          child: Text("Cancelar"),
                        ),
                        SizedBox(width: 8.0),
                        RaisedButton(
                            onPressed: () {
                              _notification();
                              Navigator.of(context).pop();
                            },
                            child: Text("Confirmar")),
                        SizedBox(width: 8.0),
                      ]))
            ],
          ),
        ),
      ],
    );
  }
}
